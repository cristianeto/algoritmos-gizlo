<?php
$arreglo = array (
    1,
    2,
    5,
    8,
    array(2,3,array(1,2))
);

/**
 * Funcion Recursiva sumarArreglo
 * 
 * $arreglo es el array inicial que contiene los valor a sumar
 * $resultado es la suma de los valores del arreglo, inicialmente es 0
 * return $resultado;
 */
function sumarArreglo($arreglo, $resultado){
  //Recorrer el array
    foreach ($arreglo as $value) {
      //Sumar si los valores NO son arreglos
        if(!is_array($value)){
            $resultado = $resultado + $value;
        }else{
          //Lamar recursivamente a la función sumarArreglo en caso de que el valor de $arreglo sea un array
            $resultado = sumarArreglo($value, $resultado);
        }
    }
    return $resultado;
}

$suma = sumarArreglo($arreglo, $resultado = 0);
echo "La suma del arreglo es: ". $suma;
?>
