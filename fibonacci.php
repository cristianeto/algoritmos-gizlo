<?php

// la función readline permite leer por entrada de teclado
$number = readline("Ingrese un número: ");

$firstValue = 0;
$secondValue = 1;
$result = 0;

// Generar fibonacci hasta el numero ingresado.
for ($i=0; $i<$number; $i++){
    echo $firstValue . " ";
    $result = $firstValue + $secondValue;
    $firstValue = $secondValue;
    $secondValue = $result;
}
?>
